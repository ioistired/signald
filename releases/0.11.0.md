# signald 0.11.0

[see all code changes here](https://gitlab.com/thefinn93/signald/-/compare/0.10.0...0.11.0)

This release brings libsignal up to date and adds support for groups v2. It also includes a number of new request types
and other features. 

I want to start by thanking contributors. Big thanks to:
* [!19](https://gitlab.com/thefinn93/signald/-/merge_requests/19) *Tulir Asokan* fixed a critical bug that arose from the libsignal update. 
* [!26](https://gitlab.com/thefinn93/signald/-/merge_requests/26) *Lazlo Westerhof* added support for explicitly sending delivery receipts
* [!28](https://gitlab.com/thefinn93/signald/-/merge_requests/28) *Lazlo Westerhof* added support for sending typing start/stop messages.
* [!29](https://gitlab.com/thefinn93/signald/-/merge_requests/29) *Tulir Asokan* added support for retrieving the QR code data for fingerprints.
* [!30](https://gitlab.com/thefinn93/signald/-/merge_requests/30) *Toby Murray* provided some much-needed updates to the documentation.
* [!32](https://gitlab.com/thefinn93/signald/-/merge_requests/32) *Toby Murray* updated the `build.gradle` file.
* [!33](https://gitlab.com/thefinn93/signald/-/merge_requests/33) *Toby Murray* updated gradle to 6.7.1.
* [!37](https://gitlab.com/thefinn93/signald/-/merge_requests/37) *Toby Murray* cleaned up some build time warnings.
* [!38](https://gitlab.com/thefinn93/signald/-/merge_requests/38) *Tulir Asokan* added `listen_started` and `listen_stopped` messages to alert clients when signald is temporarily disconnected from the server.
* [!34](https://gitlab.com/thefinn93/signald/-/merge_requests/34) *Toby Murray* cleaned up the Dockerfile. Now the only volume that needs to be mounted is ` /signald`, which contains the state files, the attachments and the socket.

This release added groups v2. To send to a v2 group, simply provide a v2 group ID. Incoming requests have a new field for v2 groups.
v2 groups require a native library, originally written in rust, to function. This is only available pre-build for 64 bit x86 machines currently,
but there is active work on adding support for other architectures, primarily arm (see [#68](https://gitlab.com/thefinn93/signald/-/issues/68)).
Eventually I hope to provide builds for as many architectures as possible. There are hints that Signal is moving all of its
core functionality to rust, so the same library can be used by all platforms. This means being able to run the native
libraries will become more and more critical.

On top all that, I've been experimenting a new request processing architecture There are a number of benefits to it, but
the biggest one to client developers will most likely be the new protocol documentation.

The protocol documentation, at its core, this is simply a JSON document that describes the protocol to communicate with
signald. Most structures are documented, as well as a few requests and responses. This should allow
partially-generated signald libraries. To experiment, I have been working on a partially generated [golang client](https://gitlab.com/signald/signald-go).
It supports all documented request types and responses, and I've successfully implemented a simple bot with it.
Additionally, I was able to render a simple [static documentation site](https://docs.signald.org/), which gets updated
automatically when changes are pushed to signald.

This also changes how incoming requests are handled inside signald. Instead of giant file with a giant switch statement
in it, kicking off one of a giant list of private methods, the new structure has each request type as its own class. 
This means that each request must declare a separate list of required arguments, including which ones are required. If
required fields are missing, the request is rejected before the code can run, improving the experience for the user while
not requiring significantly more work from developers.

Some other important announcements:
* signald's git repo will be moved to the signald organization shortly after this release. It will be available at https://gitlab.com/signald/signald.git
* signald's primary branch will be changed to `main` shortly after this release
